<?php

namespace App\Http\Controllers;

use App\Exceptions\ApplicationException;
use App\Services\ContractServiceInterface;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    /**
     * @var ContractServiceInterface
     */
    private $contractService;

    /**
     * ContractController constructor.
     *
     * @param ContractServiceInterface $contractService
     */
    public function __construct(
        ContractServiceInterface $contractService
    )
    {
        $this->contractService = $contractService;
    }

    /**
     * Get all contracts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return $this->success($this->contractService->all());
    }

    /**
     * Create a contract
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required|integer',
            'user_id' => 'required|integer',
            'title' => 'required|string',
            'description' => 'required|string',
            'file' => 'sometimes|required|mimes:pdf'
        ]);

        return $this->success($this->contractService->create($request->all()));
    }

    /**
     * Get a contract by id
     *
     * @param int|null $id Contract id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApplicationException
     */
    public function find(int $id = null)
    {
        if (is_null($id)) {
            throw new ApplicationException('Id field is required.');
        }

        return $this->success($this->contractService->getContractById($id));
    }

    /**
     * Delete a contract
     *
     * @param int|null $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApplicationException
     */
    public function delete(int $id = null)
    {
        if (is_null($id)) {
            throw new ApplicationException('Id field is required.');
        }

        return $this->success($this->contractService->deleteContract($id));
    }
}
