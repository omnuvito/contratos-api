<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpFoundation\Response as ResponseCode;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Return logged user
     *
     * @param string $token
     * @return array
     */
    protected function getUser(string $token) : array
    {
        $user = jwt_decode_token($token);

        return $user->data;
    }

    /**
     * Return Success response
     *
     * @param mixed $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function success($data) : JsonResponse
    {
        return response()->json([
            'status' => 'OK',
            'data' => $data
        ], ResponseCode::HTTP_OK);
    }
}
