<?php

namespace App\Http\Controllers;

use App\Exceptions\ApplicationException;
use App\Services\ContractProcedureServiceInterface;
use Illuminate\Http\Request;

class ContractProcedureController extends Controller
{
    /**
     * @var ContractProcedureServiceInterface
     */
    private $contractProcedureService;

    /**
     * ContractProcedureController constructor.
     *
     * @param ContractProcedureServiceInterface $contractProcedureService
     */
    public function __construct(
        ContractProcedureServiceInterface $contractProcedureService
    )
    {
        $this->contractProcedureService = $contractProcedureService;
    }

    /**
     * Get all contract procedures
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return $this->success($this->contractProcedureService->all());
    }

    /**
     * Create a contract procedure for contract approving
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'contract_id' => 'required|integer',
            'user_id' => 'required|integer',
            'file' => 'sometimes|required|mimes:pdf'
        ]);

        return $this->success($this->contractProcedureService->create(
            $request->get('contract_id'),
            $request->get('user_id'),
            $request->get('file')
        ));
    }

    /**
     * Approve a contract procedure
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function approve(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'user_id' => 'required|integer'
        ]);

        return $this->success(
            $this->contractProcedureService->approve($request->get('id'), $request->get('user_id'))
        );
    }

    /**
     * Get a contract procedure by id
     *
     * @param int|null $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApplicationException
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function find(int $id = null)
    {
        if (is_null($id)) {
            throw new ApplicationException('id field is required.');
        }

        return $this->success($this->contractProcedureService->getById($id));
    }

    /**
     * Reject a contract procedure
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function reject(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'user_id' => 'required|integer'
        ]);

        return $this->success(
            $this->contractProcedureService->reject($request->get('id'), $request->get('user_id'))
        );
    }
}
