<?php

namespace App\Http\Controllers;

use App\Exceptions\ApplicationException;
use App\Services\ContractDocumentServiceInterface;
use Illuminate\Http\Request;

class ContractDocumentController extends Controller
{
    /**
     * @var ContractDocumentServiceInterface
     */
    private $contractDocumentService;

    /**
     * ContractDocumentController constructor.
     *
     * @param ContractDocumentServiceInterface $contractDocumentService
     */
    public function __construct(
        ContractDocumentServiceInterface $contractDocumentService
    )
    {
        $this->contractDocumentService = $contractDocumentService;
    }

    /**
     * Get all contract documents
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return $this->success($this->contractDocumentService->all());
    }

    /**
     * Download a contract document
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function download(Request $request)
    {
        $this->validate($request, [
            'contract_id' => 'required|integer',
            'code' => 'required|string'
        ]);

        return response()->download(
            storage_path($this->contractDocumentService->downloadPath(
                $request->get('contract_id'),
                $request->get('code')
            ))
        );
    }

    /**
     * Upload a document for a contract procedure
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'file' => 'required|mimes:pdf',
            'user_id' => 'required|integer'
        ]);

        return $this->success($this->contractDocumentService->upload(
            $request->get('id'),
            $request->file('file'),
            $request->get('user_id')
        ));
    }

    /**
     * Approve a contract document
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function approve(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'user_id' => 'required|integer'
        ]);

        return $this->success(
            $this->contractDocumentService->approve($request->get('id'), $request->get('user_id'))
        );
    }

    /**
     * Reject a contract document
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function reject(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'user_id' => 'required|integer'
        ]);

        return $this->success(
            $this->contractDocumentService->reject($request->get('id'), $request->get('user_id'))
        );
    }
}
