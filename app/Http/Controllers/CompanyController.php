<?php

namespace App\Http\Controllers;

use App\Exceptions\ApplicationException;
use App\Services\CompanyServiceInterface;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * @var CompanyServiceInterface
     */
    private $companyService;

    /**
     * CompanyController constructor.
     *
     * @param CompanyServiceInterface $companyService
     */
    public function __construct(
        CompanyServiceInterface $companyService
    )
    {
        $this->companyService = $companyService;
    }

    /**
     * Return all the companies
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return $this->success($this->companyService->all());
    }

    /**
     * Create a new company
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'sometimes|required|string',
            'file' => 'mimes:png|nullable'
        ]);

        return $this->success(
            $this->companyService->create($request->get('name'), $request->get('description'), $request->file('file'))
        );
    }

    /**
     * Upload Company Logo
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function uploadLogo(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required|integer',
            'file' => 'required|mimes:png'
        ]);

        return $this->success(
            $this->companyService->uploadLogo($request->get('company_id'), $request->get('file'))
        );
    }

    /**
     * Find a company by id
     *
     * @param int|null $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApplicationException
     */
    public function find(int $id = null)
    {
        if (is_null($id)) {
            throw new ApplicationException('Id field is required.');
        }

        return $this->success($this->companyService->getCompanyById($id));
    }

    /**
     * Update company information
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        //TODO: develop update function.
        return $this->success($this->companyService->update($request->all()));
    }
}
