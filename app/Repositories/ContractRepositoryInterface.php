<?php

namespace App\Repositories;

use App\Models\Contract;
use Illuminate\Database\Eloquent\Collection;

interface ContractRepositoryInterface
{
    /**
     * Create a new contract
     *
     * @param array $data Data to create a new company
     * @return \App\Models\Contract
     */
    public function create(array $data) : Contract;

    /**
     * Update contract info
     *
     * @param array $data Data to update contract
     * @param int $id Contract id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(array $data, int $id) : bool;

    /**
     * Delete a contract
     *
     * @param int $id Contract id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function delete(int $id) : bool;

    /**
     * Find a contract by id
     *
     * @param int $id Contract id
     * @return \App\Models\Contract|null
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id) : ?Contract;

    /**
     * Return all contracts
     *
     * @return \App\Models\Contract[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all() : Collection;
}
