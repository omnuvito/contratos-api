<?php

namespace App\Repositories;

use App\Models\ContractDocument;
use Illuminate\Database\Eloquent\Collection;

interface ContractDocumentRepositoryInterface
{
    /**
     * Create a new contract document
     *
     * array['contract_id'] int Contract procedure id
     * array['code'] string Contract document name, by default: CONT
     * array['name'] string Contract name, by default: Contract.
     * array['uploaded'] bool Check if a contract document was uploaded, by default: false.
     * array['created_by'] int User id, this is the user that create the contract document
     * array['updated_by'] int User id, this is the user that update the contract document
     *
     * @param array $data (see above)
     * @return \App\Models\ContractDocument
     */
    public function create(array $data) : ContractDocument;

    /**
     * Update contract document info
     *
     * @param array $data Data to update contract document
     * @param int $id Contract document id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(array $data, int $id) : bool;

    /**
     * Delete a contract document
     *
     * @param int $id Contract document id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function delete(int $id) : bool;

    /**
     * Find a contract document by id
     *
     * @param int $id Contract document id
     * @return \App\Models\ContractDocument|null
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id) : ?ContractDocument;

    /**
     * Return all contract documents
     *
     * @return \App\Models\ContractDocument[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all() : Collection;
}
