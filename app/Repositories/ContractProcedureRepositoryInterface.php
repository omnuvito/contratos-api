<?php

namespace App\Repositories;

use App\Models\ContractProcedure;
use Illuminate\Database\Eloquent\Collection;

interface ContractProcedureRepositoryInterface
{
    /**
     * Create a new contract procedure
     *
     * @param array $data Data to create a new contract procedure
     * @return \App\Models\ContractProcedure
     */
    public function create(array $data) : ContractProcedure;

    /**
     * Update contract procedure info
     *
     * @param array $data Data to update contract procedure
     * @param int $id Contract procedure id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(array $data, int $id) : bool;

    /**
     * Delete a contract procedure
     *
     * @param int $id Contract procedure id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function delete(int $id) : bool;

    /**
     * Find a contract procedure by id
     *
     * @param int $id Contract procedure id
     * @return \App\Models\ContractProcedure|null
     */
    public function findById(int $id) : ?ContractProcedure;

    /**
     * Return all contract procedures
     *
     * @return \App\Models\ContractProcedure[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all() : Collection;

    /**
     * Find a contract procedure by company id and status
     *
     * @param int $companyId Company id
     * @param string $status Contract procedure status
     * @return \App\Models\ContractProcedure[]|\Illuminate\Database\Eloquent\Collection
     */
    public function findByCompanyIdAndStatus(int $companyId, string $status) : array;
}
