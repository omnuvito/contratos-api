<?php

namespace App\Repositories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Collection;

interface CompanyRepositoryInterface
{
    /**
     * Create a new company
     *
     * array['name'] string Company name
     * array['logo'] string Company logo name, eg: logo.png
     * array['description'] string Company description: about it, services, etc.
     *
     * @param array $data (see above)
     * @return \App\Models\Company
     */
    public function create(array $data) : Company;

    /**
     * Update company info
     *
     * @param array $data Data to update company
     * @param int $id Company id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(array $data, int $id) : bool;

    /**
     * Delete a company
     *
     * @param int $id Company id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function delete(int $id) : bool;

    /**
     * Find a company by id
     *
     * @param int $id Company id
     * @return \App\Models\Company|null
     */
    public function findById(int $id) : ?Company;

    /**
     * Return all companies
     *
     * @return \App\Models\Company[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all() : Collection;
}
