<?php

namespace App\Repositories\Impl;

use App\Models\Contract;
use App\Repositories\ContractRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ContractRepository
    implements ContractRepositoryInterface
{
    /**
     * @var Contract
     */
    private $contract;

    /**
     * ContractRepository constructor.
     *
     * @param Contract $contract
     */
    public function __construct(
        Contract $contract
    )
    {
        $this->contract = $contract;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data) : Contract
    {
        return $this->contract->create($data);
    }

    /**
     * @inheritDoc
     */
    public function update(array $data, int $id) : bool
    {
        return $this->contract->whereId($id)->update($data);
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id) : bool
    {
        return $this->contract->destroy($id);
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id) : ?Contract
    {
        return $this->contract->with('document')->find($id);
    }

    /**
     * @inheritDoc
     */
    public function all() : Collection
    {
        return $this->contract->all();
    }
}
