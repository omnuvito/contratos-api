<?php

namespace App\Repositories\Impl;

use App\Models\Company;
use App\Repositories\CompanyRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CompanyRepository
    implements CompanyRepositoryInterface
{
    /**
     * @var Company
     */
    private $company;

    /**
     * CompanyRepository constructor.
     *
     * @param Company $company
     */
    public function __construct(
        Company $company
    )
    {
        $this->company = $company;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data) : Company
    {
        return $this->company->create($data);
    }

    /**
     * @inheritDoc
     */
    public function update(array $data, int $id) : bool
    {
        return $this->company->whereId($id)->update($data);
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id) : bool
    {
        return $this->company->destroy($id);
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id) : ?Company
    {
        return $this->company->with('contracts.createdBy', 'users')->find($id);
    }

    /**
     * @inheritDoc
     */
    public function all() : Collection
    {
        return $this->company->all();
    }
}
