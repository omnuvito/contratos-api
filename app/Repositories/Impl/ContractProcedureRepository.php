<?php

namespace App\Repositories\Impl;

use App\Models\ContractProcedure;
use App\Repositories\ContractProcedureRepositoryInterface;
use \Illuminate\Database\Eloquent\Collection;

class ContractProcedureRepository
    implements ContractProcedureRepositoryInterface
{
    /**
     * @var ContractProcedure
     */
    private $contractProcedure;

    /**
     * ContractProcedureRepository constructor.
     *
     * @param ContractProcedure $contractProcedure
     */
    public function __construct(
        ContractProcedure $contractProcedure
    )
    {
        $this->contractProcedure = $contractProcedure;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data) : ContractProcedure
    {
        return $this->contractProcedure->create($data);
    }

    /**
     * @inheritDoc
     */
    public function update(array $data, int $id) : bool
    {
        return $this->contractProcedure->whereId($id)->update($data);
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id) : bool
    {
        return $this->contractProcedure->destroy($id);
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id) : ?ContractProcedure
    {
        return $this->contractProcedure->with('contract', 'createdBy', 'updatedBy')->find($id);
    }

    /**
     * @inheritDoc
     */
    public function all() : Collection
    {
        return $this->contractProcedure->all();
    }

    /**
     * @inheritDoc
     */
    public function findByCompanyIdAndStatus(int $id, string $status) : array
    {
        return $this->contractProcedure->whereId($id)
            ->whereStatus($status)
            ->get();
    }
}
