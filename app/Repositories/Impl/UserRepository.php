<?php

namespace App\Repositories\Impl;

use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class UserRepository
    implements UserRepositoryInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * UserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(
        User $user
    )
    {
        $this->user = $user;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data) : User
    {
        return $this->user->create($data);
    }

    /**
     * @inheritDoc
     */
    public function update(array $data, int $id) : bool
    {
        return $this->user->whereId($id)->update($data);
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id) : bool
    {
        return $this->user->destroy($id);
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id) : ?User
    {
        return $this->user->find($id)->first();
    }

    /**
     * @inheritDoc
     */
    public function all() : Collection
    {
        return $this->user->all();
    }

    /**
     * @inheritDoc
     */
    public function findByEmail(string $email) : ?User
    {
        return $this->user->with('company')
            ->whereEmail($email)
            ->first();
    }
}
