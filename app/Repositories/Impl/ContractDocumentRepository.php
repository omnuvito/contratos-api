<?php

namespace App\Repositories\Impl;

use App\Models\ContractDocument;
use App\Repositories\ContractDocumentRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ContractDocumentRepository
    implements ContractDocumentRepositoryInterface
{
    /**
     * @var ContractDocument
     */
    private $contractDocument;

    /**
     * ContractDocumentRepository constructor.
     *
     * @param ContractDocument $contractDocument
     */
    public function __construct(
        ContractDocument $contractDocument
    )
    {
        $this->contractDocument = $contractDocument;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data) : ContractDocument
    {
        return $this->contractDocument->create($data);
    }

    /**
     * @inheritDoc
     */
    public function update(array $data, int $id) : bool
    {
        return $this->contractDocument->whereId($id)->update($data);
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id) : bool
    {
        return $this->contractDocument->destroy($id);
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id) : ?ContractDocument
    {
        return $this->contractDocument->findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function all() : Collection
    {
        return $this->contractDocument->all();
    }
}
