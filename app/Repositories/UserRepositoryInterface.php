<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface UserRepositoryInterface
{
    /**
     * Create a new user
     *
     * @param array $data Data to create a new user
     * @return \App\Models\User
     */
    public function create(array $data) : User;

    /**
     * Update user info
     *
     * @param array $data Data to update user
     * @param int $id User id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(array $data, int $id) : bool;

    /**
     * Delete a user
     *
     * @param int $id User id
     * @return bool
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function delete(int $id) : bool;

    /**
     * Find an user by id
     *
     * @param int $id User id
     * @return \App\Models\User|null
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(int $id) : ?User;

    /**
     * Return all users
     *
     * @return \App\Models\Contract[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all() : Collection;

    /**
     * Find a user by email
     *
     * @param string $email User email
     * @return \App\Models\User|null
     */
    public function findByEmail(string $email) : ?User;
}
