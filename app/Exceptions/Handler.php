<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Log;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        TokenMismatchException::class,
    ];

    /**
     * Errors list.
     *
     * @var array
     */
    private $errors = [
        [
            'class' => ModelNotFoundException::class,
            'errorMessage' => 'Model not found.',
            'responseCode' => ResponseCode::HTTP_NOT_FOUND
        ],
        [
            'class' => ApplicationException::class,
            'errorMessage' => 'There was a request error.',
            'responseCode' => ResponseCode::HTTP_BAD_REQUEST
        ],
        [
            'class' => ResourceNotFoundException::class,
            'errorMessage' => 'Resource not found.',
            'responseCode' => ResponseCode::HTTP_NOT_FOUND
        ],
        [
            'class' => UnauthorizedException::class,
            'errorMessage' => 'Invalid authentication.',
            'responseCode' => ResponseCode::HTTP_UNAUTHORIZED
        ]
    ];

    /**
     * Render an exception into a HTTP response.
     *
     * @param  Request $request
     * @param  Exception $e
     * @return JsonResponse|ResponseCode|void
     */
    public function render($request, Exception $e)
    {
        Log::info("Handling Exception " . get_class($e));

        if ($error = $this->exceptionExists($e)) {
            if (isset($error['postExecution']) && method_exists($e, $error['postExecution'])) {
                try {
                    call_user_func([$e, $error['postExecution']]);
                } catch (Exception $e) {
                    Log::error("Error calling function exec.");
                    Log::error($e->getMessage());
                    Log::error($e->getTraceAsString());
                }
            }

            return $this->getResponse($e, $request, $error);
        }

        return $this->getResponse($e, $request);
    }

    /**
     * Return error response.
     *
     * @param string $message error message
     * @param int $code error code
     * @param null|string $infoCode information's code
     * @return JsonResponse
     */
    protected function getErrorResponse($message, $code, $infoCode = null)
    {
        if (! $infoCode) {
            $infoCode = $code;
        }

        if (! is_array($message)) {
            $message = [$message];
        }

        return response()->json([
            'errors' => [
                'code' => $infoCode,
                'message' => $message
            ]
        ], $code);
    }

    /**
     * Check if an error exists into the errors array.
     *
     * @param Exception $e Exception to find into the array.
     * @return bool
     */
    private function exceptionExists(Exception $e)
    {
        foreach ($this->errors as $error) {
            if ($e instanceof $error['class']) {
                return $error;
            }
        }

        return false;
    }

    /**
     * Set the message.
     *
     * @param Exception $e
     * @param $request
     * @param null $error
     * @return JsonResponse
     */
    private function getResponse(Exception $e, $request, $error = null)
    {
        if (! is_null($error)) {
            $errorMessage = $this->getMessageError($e, $error);
            $errorCode = $this->getErrorCode($e, $error);
            $responseCode = $this->getResponseCode($e, $error);
            $response = $this->getErrorResponse($errorMessage, $responseCode, $errorCode);
        } elseif (method_exists($e, "getStatusCode")) {
            $response = $this->getErrorResponse('There was an error.', $e->getStatusCode());
        } else {
            $response = $this->getErrorResponse('There was an error.', 500);
        }

        return $response;
    }

    /**
     * Retorna Mensaje de error.
     *
     * @param Exception $e
     * @param array $error
     * @return string
     */
    private function getMessageError(Exception $e, $error)
    {
        return ($e->getMessage() === '' && isset($error['errorMessage'])) ? $error['errorMessage'] : $e->getMessage();
    }

    /**
     * Return response code.
     *
     * @param Exception $e
     * @param array $error
     * @return int|mixed
     */
    private function getResponseCode(Exception $e, $error)
    {
        return ($e->getCode() === 0 && isset($error['responseCode'])) ? $error['responseCode'] : $e->getCode();
    }

    /**
     * Retur code error.
     *
     * @param Exception $e
     * @param array $error
     * @return int
     */
    private function getErrorCode(Exception $e, $error)
    {
        return ($e->getCode() === 0 && isset($error['errorCode'])) ? $error['errorCode'] : $e->getCode();
    }
}
