<?php

namespace App\Services;

use App\Models\Contract;
use Illuminate\Database\Eloquent\Collection;

interface ContractServiceInterface
{
    /**
     * Create a new contract
     *
     * array['company_id'] int Company Id
     * array['user_id'] int User id, this is the user that create the contract, it will be used to created_by and updated_by
     * array['title'] string Contract title
     * array['description'] string Contract description
     * array['file'] UploadedFile Contract document
     *
     * @param array $data (see above)
     * @return string
     */
    public function create(array $data) : string;

    /**
     * Get all contracts
     *
     * @return \App\Models\ContractProcedure[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all() : Collection;

    /**
     * Get a contract by id
     *
     * @param int $id Contract Id
     * @return \App\Models\Contract
     */
    public function getContractById(int $id) : Contract;

    /**
     * Delete a contract
     *
     * @param int $id Contract id
     * @return string
     */
    public function deleteContract(int $id) : string;
}
