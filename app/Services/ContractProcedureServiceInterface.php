<?php

namespace App\Services;

use App\Models\ContractProcedure;
use Illuminate\Database\Eloquent\Collection;

interface ContractProcedureServiceInterface
{
    /**
     * Get all contract procedures
     *
     * @return \App\Models\ContractProcedure[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all() : Collection;

    /**
     * Get all contract procedures by company id
     *
     * @param int $companyId Company id
     * @param string $status Contract procedure status
     * @return \App\Models\ContractProcedure[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByCompany(int $companyId, string $status) : Collection;

    /**
     * Create a contract procedure
     *
     * @param int $contractId Contract id
     * @param int $userId User Id, this is the user that create the contract procedure
     * @return string
     */
    public function create(int $contractId, int $userId) : string;

    /**
     * Approve a contract procedure
     *
     * @param int $id Contract procedure id
     * @param int $userId User id, this is the user that approve the procedure
     * @return string
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function approve(int $id, int $userId) : string;

    /**
     * Reject a contract procedure
     *
     * @param int $id Contract procedure id
     * @param int $userId User id, this is the user that reject the procedure
     * @return string
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function reject(int $id, int $userId) : string;

    /**
     * Get a contract procedure by id
     *
     * @param int $id Contract procedure id
     * @return \App\Models\ContractProcedure
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function getById(int $id) : ContractProcedure;
}
