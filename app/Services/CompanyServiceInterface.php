<?php

namespace App\Services;

use App\Models\Company;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;

interface CompanyServiceInterface
{
    /**
     * Get all companies
     *
     * @return \App\Models\Company[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all() : Collection;

    /**
     * Create a new company
     *
     * @param string $name Company name
     * @param string|null $description Company description
     * @param UploadedFile $file Company logo image
     * @return \App\Models\Company
     */
    public function create(string $name, string $description = null, UploadedFile $file = null) : Company;

    /**
     * Update a company
     *
     * @param array $data Company data to update
     * @param int $id Company id
     * @return string
     */
    public function update(array $data, int $id) : string;

    /**
     * Delete a company
     *
     * @param int $id Company id
     * @return string
     */
    public function delete(int $id) : string;

    /**
     * Return a company by its id
     *
     * @param int $id Company id
     * @return \App\Models\Company
     */
    public function getCompanyById(int $id) : Company;

    /**
     * Upload Company Logo
     *
     * @param int $companyId Company Id
     * @param UploadedFile $file Logo image file
     * @return string
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function uploadLogo(int $companyId, UploadedFile $file) : string;
}
