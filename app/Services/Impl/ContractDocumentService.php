<?php

namespace App\Services\Impl;

use App\Exceptions\ResourceNotFoundException;
use App\Models\ContractDocument;
use App\Repositories\ContractDocumentRepositoryInterface;
use App\Services\ContractDocumentServiceInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ContractDocumentService
    implements ContractDocumentServiceInterface
{
    /**
     * EXCEPTIONS MSGS
     */
    const DOCUMENT_DOES_NOT_EXISTS = 'Document does not exists.';

    /**
     * @var ContractDocumentRepositoryInterface
     */
    private $contractDocumentoRepository;

    /**
     * ContractDocumentService constructor.
     *
     * @param ContractDocumentRepositoryInterface $contractDocumentRepository
     */
    public function __construct(
        ContractDocumentRepositoryInterface $contractDocumentRepository
    )
    {
        $this->contractDocumentoRepository = $contractDocumentRepository;
    }

    /**
     * @inheritDoc
     */
    public function all() : Collection
    {
        return $this->contractDocumentoRepository->all();
    }

    /**
     * @inheritDoc
     */
    public function create(array $data) : ContractDocument
    {
        return $this->contractDocumentoRepository->create($data);
    }

    /**
     * @inheritDoc
     */
    public function downloadPath(int $contractId, string $code) : string
    {
        if (! Storage::disk('contracts')->exists("{$contractId}/{$code}.pdf")) {
            throw new ResourceNotFoundException(self::DOCUMENT_DOES_NOT_EXISTS);
        }

        return "contracts/{$contractId}/{$code}.pdf";
    }

    /**
     * @inheritDoc
     */
    public function upload(int $id, UploadedFile $file, int $userId) : string
    {
        DB::transaction(function() use($id, $file, $userId) {
            if (is_null($contractDocument = $this->contractDocumentoRepository->findById($id))) {
                throw new ResourceNotFoundException(self::DOCUMENT_DOES_NOT_EXISTS);
            }

            $data = [
                'uploaded' => true,
                'updated_by' => $userId
            ];

            $this->contractDocumentoRepository->update($data, $contractDocument->id);

            Storage::disk('public')
                ->putFileAs(
                    'company' . DIRECTORY_SEPARATOR . 'contracts' . DIRECTORY_SEPARATOR . $contractDocument->contract->company->id,
                    $file,
                    "{$contractDocument->code}.pdf"
                );
        });

        return 'The document contract has been uploaded successfully';
    }

    /**
     * @inheritDoc
     */
    public function approve(int $id, int $userId) : string
    {
        if (! $this->contractDocumentoRepository->findById($id)) {
            throw new ResourceNotFoundException(self::DOCUMENT_DOES_NOT_EXISTS);
        }

        $data = [
            'status' => 'APPROVED',
            'updated_by' => $userId
        ];

        $this->contractDocumentoRepository->update($data, $id);

        return 'The document contract has been approved.';
    }

    /**
     * @inheritDoc
     */
    public function reject(int $id, int $userId) : string
    {
        if (! $this->contractDocumentoRepository->findById($id)) {
            throw new ResourceNotFoundException(self::DOCUMENT_DOES_NOT_EXISTS);
        }

        $data = [
            'status' => 'REJECTED',
            'updated_by' => $userId
        ];

        $this->contractDocumentoRepository->update($data, $id);

        return 'The document contract has been rejected.';
    }
}
