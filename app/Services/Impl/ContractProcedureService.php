<?php

namespace App\Services\Impl;

use App\Exceptions\ApplicationException;
use App\Exceptions\ResourceNotFoundException;
use App\Models\ContractProcedure;
use App\Repositories\ContractDocumentRepositoryInterface;
use App\Repositories\ContractProcedureRepositoryInterface;
use App\Repositories\ContractRepositoryInterface;
use App\Services\ContractProcedureServiceInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ContractProcedureService
    implements ContractProcedureServiceInterface
{
    /**
     * EXCEPTIONS MSGS
     */
    const PROCEDURE_DOES_NOT_EXISTS = 'Contract procedure does not exists.';

    /**
     * @var ContractDocumentRepositoryInterface
     */
    private $contractDocumentRepository;

    /**
     * @var ContractProcedureRepositoryInterface
     */
    private $contractProcedureRepository;

    /**
     * @var ContractRepositoryInterface
     */
    private $contractRepository;

    /**
     * ContractProcedureService constructor.
     *
     * @param ContractDocumentRepositoryInterface $contractDocumentRepository
     * @param ContractProcedureRepositoryInterface $contractProcedureRepository
     * @param ContractRepositoryInterface $contractRepository
     */
    public function __construct(
        ContractDocumentRepositoryInterface $contractDocumentRepository,
        ContractProcedureRepositoryInterface $contractProcedureRepository,
        ContractRepositoryInterface $contractRepository
    )
    {
        $this->contractDocumentRepository = $contractDocumentRepository;
        $this->contractProcedureRepository = $contractProcedureRepository;
        $this->contractRepository = $contractRepository;
    }

    /**
     * @inheritDoc
     */
    public function all() : Collection
    {
        return $this->contractProcedureRepository->all();
    }

    /**
     * @inheritDoc
     */
    public function create(int $contractId, int $userId) : string
    {
        DB::transaction(function() use($contractId, $userId) {
            $contractProcedureData = [
                'contract_id' => $contractId,
                'created_by' => $userId,
                'updated_by' => $userId
            ];

            $this->contractProcedureRepository->create($contractProcedureData);
        });

        return 'Contract Procedure created successfully';
    }

    /**
     * @inheritDoc
     */
    public function approve(int $id, int $userId) : string
    {
        DB::transaction(function() use($id, $userId) {
            $procedure = $this->contractProcedureRepository->findById($id);

            if (is_null($procedure)) {
                throw new ResourceNotFoundException(self::PROCEDURE_DOES_NOT_EXISTS);
            }

            if ($procedure->status !== 'PENDING') {
                throw new ApplicationException('This procedure is already completed.');
            }

            if ($procedure->contract->document->status === 'PENDING') {
                throw new ApplicationException('Procedure with contract document not approved.');
            }

            $data = [
                'status' => 'APPROVED',
                'updated_by' => $userId
            ];

            $this->contractProcedureRepository->update($data, $id);

            $this->contractRepository->update(['is_approved' => true], $procedure->contract->id);
        });

        return 'The procedure has been approved.';
    }

    /**
     * @inheritDoc
     */
    public function reject(int $id, int $userId) : string
    {
        if (! $this->contractProcedureRepository->findById($id)) {
            throw new ResourceNotFoundException(self::PROCEDURE_DOES_NOT_EXISTS);
        }

        $data = [
            'status' => 'REJECTED',
            'updated_by' => $userId
        ];

        $this->contractProcedureRepository->update($data, $id);

        return 'The procedure has been rejected.';
    }

    /**
     * @inheritDoc
     */
    public function getByCompany(int $companyId, string $status) : Collection
    {
        return $this->contractProcedureRepository->findByCompanyIdAndStatus($companyId, $status);
    }

    /**
     * @inheritDoc
     */
    public function getById(int $id) : ContractProcedure
    {
        if (is_null($contractProcedure = $this->contractProcedureRepository->findById($id))) {
            throw new ResourceNotFoundException('Contract procedure does not exists.');
        }

        return $contractProcedure;
    }
}
