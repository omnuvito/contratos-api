<?php

namespace App\Services\Impl;

use App\Models\Contract;
use App\Repositories\CompanyRepositoryInterface;
use App\Repositories\ContractDocumentRepositoryInterface;
use App\Repositories\ContractRepositoryInterface;
use App\Services\ContractProcedureServiceInterface;
use App\Services\ContractServiceInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;

class ContractService
    implements ContractServiceInterface
{
    /**
     * @var CompanyRepositoryInterface
     */
    private $companyRepository;

    /**
     * @var ContractRepositoryInterface
     */
    private $contractRepository;

    /**
     * @var ContractDocumentRepositoryInterface
     */
    private $contractDocumentRepository;

    /**
     * @var ContractProcedureServiceInterface
     */
    private $contractProcedureService;

    /**
     * ContractService constructor.
     *
     * @param CompanyRepositoryInterface $companyRepository
     * @param ContractDocumentRepositoryInterface $contractDocumentRepository
     * @param ContractRepositoryInterface $contractRepository
     * @param ContractProcedureServiceInterface $contractProcedureService
     */
    public function __construct(
        CompanyRepositoryInterface $companyRepository,
        ContractDocumentRepositoryInterface $contractDocumentRepository,
        ContractRepositoryInterface $contractRepository,
        ContractProcedureServiceInterface $contractProcedureService
    )
    {
        $this->companyRepository = $companyRepository;
        $this->contractDocumentRepository = $contractDocumentRepository;
        $this->contractRepository = $contractRepository;
        $this->contractProcedureService = $contractProcedureService;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data) : string
    {
        DB::transaction(function () use ($data) {
            $file = $data['file'];
            $userId = $data['user_id'];

            unset($data['file']);

            $data['created_by'] = $userId;
            $data['updated_by'] = $userId;

            $contract = $this->contractRepository->create($data);

            $contractDocumentData = [
                'contract_id' => $contract->id,
                'created_by' => $userId,
                'updated_by' => $userId
            ];

            $contractDocument = $this->contractDocumentRepository->create($contractDocumentData);

            $this->contractProcedureService->create($contract->id, $userId);

            if (! is_null($file)) {
                Storage::disk('public')
                    ->putFileAs(
                        'company' . DIRECTORY_SEPARATOR . 'contracts' . DIRECTORY_SEPARATOR . $contract->company->id,
                        $file,
                        "CONT.pdf"
                    );

                $this->contractDocumentRepository->update(['uploaded' => true], $contractDocument->id);
            }
        });

        return 'Contract created successfully';
    }

    /**
     * @inheritDoc
     */
    public function all() : Collection
    {
        return $this->contractRepository->all();
    }

    /**
     * @inheritDoc
     */
    public function getContractById(int $id) : Contract
    {
        return $this->contractRepository->findById($id);
    }

    /**
     * @inheritDoc
     */
    public function deleteContract(int $id) : string
    {
        //TODO: hacer transaction para borrar contracto y documento.
        return DB::transaction(function() use($id) {
            if ($this->contractRepository->delete($id)) {
                return 'Contract successfully deleted.';
            }

            return 'Contract not deleted.';
        });
    }
}
