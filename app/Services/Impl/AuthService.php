<?php

namespace App\Services\Impl;

use App\Exceptions\ApplicationException;
use App\Exceptions\ResourceNotFoundException;
use App\Repositories\UserRepositoryInterface;
use App\Services\AuthServiceInterface;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class AuthService
    implements AuthServiceInterface
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * AuthService constructor.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @inheritDoc
     */
    public function login(string $email, string $password) : array
    {
        $user = $this->userRepository->findByEmail($email);

        if (is_null($user)) {
            throw new ResourceNotFoundException('User does not exists.');
        }

        if (! Hash::check($password, $user->password)) {
            throw new ApplicationException('Invalid Password.');
        }

        $userData = Arr::except($user->toArray(), ['created_at', 'updated_at', 'deleted_at', 'password']);

        $token = jwt_build_token($userData);

        return [
            'token' => $token,
            'user' => $user
        ];
    }
}
