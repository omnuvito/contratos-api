<?php

namespace App\Services\Impl;

use App\Exceptions\ResourceNotFoundException;
use App\Models\Company;
use App\Repositories\CompanyRepositoryInterface;
use App\Services\CompanyServiceInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class CompanyService
    implements CompanyServiceInterface
{
    /**
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * CompanyService constructor.
     *
     * @param CompanyRepositoryInterface $companyRepository
     */
    public function __construct(
        CompanyRepositoryInterface $companyRepository
    )
    {
        $this->companyRepository = $companyRepository;
    }

    /**
     * @inheritDoc
     */
    public function all() : Collection
    {
        return $this->companyRepository->all();
    }

    /**
     * @inheritDoc
     */
    public function create(string $name, string $description = null, UploadedFile $file = null) : Company
    {
        return DB::transaction(function() use($name, $description, $file) {
            $data['name'] = $name;

            if (! is_null($description)) {
                $data['description'] = $description;
            }

            $company = $this->companyRepository->create($data);

            if (! is_null($file)) {
                Storage::disk('public')
                    ->putFileAs('company' . DIRECTORY_SEPARATOR . 'logo' . DIRECTORY_SEPARATOR . $company->id,
                        $file,
                        'logo.png'
                    );

                $this->companyRepository->update(['logo' => 'logo.png'], $company->id);
            }

            return $company;
        });
    }

    /**
     * @inheritDoc
     */
    public function update(array $data, int $id) : string
    {
        $this->companyRepository->update($data, $id);

        return 'Company successfully updated.';
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id) : string
    {
        $this->companyRepository->delete($id);

        return 'Company successfully deleted.';
    }

    /**
     * @inheritDoc
     */
    public function getCompanyById(int $id) : Company
    {
        return $this->companyRepository->findById($id);
    }

    /**
     * @inheritDoc
     */
    public function uploadLogo(int $companyId, UploadedFile $file) : string
    {
        if (! is_null($company = $this->companyRepository->findById($companyId))) {
            throw new ResourceNotFoundException('Company does not exists.');
        }

        DB::transaction(function() use($company, $file) {
            if (! is_null($company->logo)) {
                $this->companyRepository->update(['logo' => 'logo.png'], $company->id);
            }

            Storage::disk('companies')
                ->putFileAs($company->id . DIRECTORY_SEPARATOR . 'logos', $file, 'logo.png');
        });

        return 'Logo uploaded successfully';
    }
}
