<?php

namespace App\Services;

interface AuthServiceInterface
{
    /**
     * Return logged user token
     *
     * @param string $email User email
     * @param string $password User password
     * @return array
     * @throws \App\Exceptions\ApplicationException
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function login(string $email, string $password) : array;
}
