<?php

namespace App\Services;

use App\Models\ContractDocument;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;

interface ContractDocumentServiceInterface
{
    /**
     * @return \App\Models\Company[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all() : Collection;

    /**
     * Create a new contract document
     *
     * array['contract_procedure_id'] int Contract procedure id
     * array['code'] string Contract document code, it will be use to rename uploaded file
     * array['name'] string Contract name
     * array['uploaded'] bool Check if a file was uploaded
     * array['created_by'] int User id, user that is creating the contract document
     * array['updated_by'] int User id, user that is updating the contract document
     * array['status'] string Contract document status, this will be: Pending, Rejected or Approved
     *
     * @param array $data (see above)
     * @return \App\Models\ContractDocument
     */
    public function create(array $data) : ContractDocument;

    /**
     * Upload a contract document
     *
     * @param int $id contract document id
     * @param UploadedFile $file Document File
     * @param int $userId User id, user that is uploading the document file
     * @return string
     */
    public function upload(int $id, UploadedFile $file, int $userId) : string;

    /**
     * Return download url contract document
     *
     * @param int $contractId Contract id that document belongs to.
     * @param string $code Contract document code
     * @return string
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function downloadPath(int $contractId, string $code) : string;

    /**
     * Approve a contract document
     *
     * @param int $id Contract document id
     * @param int $userId User id, user that is approving the document file
     * @return string
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function approve(int $id, int $userId) : string;

    /**
     * Reject a contract document
     *
     * @param int $id Document id
     * @param int $userId User id, user that is approving the document file
     * @return string
     * @throws \App\Exceptions\ResourceNotFoundException
     */
    public function reject(int $id, int $userId) : string;
}
