<?php

if (! function_exists('path_to_certification_documents')) {

    /**
     * Return contract document to approve
     *
     * @param int $contractId
     * @return string
     */
    function path_to_certification_documents($contractId)
    {
        //TODO: Desarrollar path relativo
        $relativePath = '' . $contractId;

        return app_path($relativePath);
    }
}
