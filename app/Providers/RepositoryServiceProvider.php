<?php

namespace App\Providers;

use App\Repositories\CompanyRepositoryInterface;
use App\Repositories\ContractDocumentRepositoryInterface;
use App\Repositories\ContractProcedureRepositoryInterface;
use App\Repositories\ContractRepositoryInterface;
use App\Repositories\Impl\CompanyRepository;
use App\Repositories\Impl\ContractDocumentRepository;
use App\Repositories\Impl\ContractProcedureRepository;
use App\Repositories\Impl\ContractRepository;
use App\Repositories\Impl\UserRepository;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            CompanyRepositoryInterface::class,
            CompanyRepository::class
        );
        $this->app->singleton(
            ContractDocumentRepositoryInterface::class,
            ContractDocumentRepository::class
        );
        $this->app->singleton(
            ContractProcedureRepositoryInterface::class,
            ContractProcedureRepository::class
        );
        $this->app->singleton(
            ContractRepositoryInterface::class,
            ContractRepository::class
        );
        $this->app->singleton(
            UserRepositoryInterface::class,
            UserRepository::class
        );
    }
}
