<?php

namespace App\Providers;

use App\Services\AuthServiceInterface;
use App\Services\CompanyServiceInterface;
use App\Services\ContractDocumentServiceInterface;
use App\Services\ContractProcedureServiceInterface;
use App\Services\ContractServiceInterface;
use App\Services\Impl\AuthService;
use App\Services\Impl\CompanyService;
use App\Services\Impl\ContractDocumentService;
use App\Services\Impl\ContractProcedureService;
use App\Services\Impl\ContractService;
use Illuminate\Support\ServiceProvider;

class ServicioServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            AuthServiceInterface::class,
            AuthService::class
        );
        $this->app->singleton(
            CompanyServiceInterface::class,
            CompanyService::class
        );
        $this->app->singleton(
            ContractDocumentServiceInterface::class,
            ContractDocumentService::class);
        $this->app->singleton(
            ContractProcedureServiceInterface::class,
            ContractProcedureService::class
        );
        $this->app->singleton(
            ContractServiceInterface::class,
            ContractService::class);
    }
}
