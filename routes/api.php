<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group([], function() {
    Route::post('/auth/login', 'AuthController@login');
});

/**
 * COMPANY
 */
Route::group([], function () {
    Route::get('/companies', 'CompanyController@all');
    Route::get('/companies/{id}', 'CompanyController@find');
    Route::post('/companies', 'CompanyController@create');
    Route::patch('/companies', 'CompanyController@update');
    Route::post('/companies/upload/logo', 'CompanyController@uploadLogo');
});

/**
 * CONTRACTS
 */
Route::group([], function () {
    Route::get('/contracts', 'ContractController@all');
    Route::get('/contracts/{id}', 'ContractController@find');
    Route::post('/contracts', 'ContractController@create');
    Route::delete('/contracts/{id}', 'ContractController@delete');
});

/**
 * CONTRACT DOCUMENTS
 */
Route::group([], function () {
    Route::get('/contract/documents', 'ContractDocumentController@all');
    Route::post('/contract/documents/upload', 'ContractDocumentController@upload');
    Route::get('/contract/documents/download/{contract_id?}/{code?}', 'ContractDocumentController@download');
    Route::post('/contract/documents/approve', 'ContractDocumentController@approve');
    Route::post('/contract/documents/reject', 'ContractDocumentController@reject');
});

/**
 * CONTRACT PROCEDURES
 */
Route::group([], function() {
    Route::get('/contract/procedures', 'ContractProcedureController@all');
    Route::get('/contract/procedures/{id}', 'ContractProcedureController@find');
    Route::post('/contract/procedures', 'ContractProcedureController@create');
    Route::post('/contract/procedures/approve', 'ContractProcedureController@approve');
    Route::post('/contract/procedures/reject', 'ContractProcedureController@reject');
});

/**
 * USERS
 */
Route::group([], function() {
    Route::post('/users', 'Auth\RegisterController@create');
});
